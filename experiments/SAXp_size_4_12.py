import numpy as np

import math
# USE LOCAL MAPEL
import sys
sys.path.append("..")
import mapel
import printing_plus as pp
import kKemenyDistances as kkd

from scoring_rule_Xp import *

name = 'SAXp_size_4_12'
experiment_id = f'{name}'
instance_type = 'ordinal'
distance_id = 'swap'
embedding_id = 'mds'

## Compute the (normalized) size of the SAXp for the Borda score
def SAXp_size(P):
    P2 = P.votes.tolist()
    n, m = np.shape(P2)
    borda_weights = [m - i - 1 for i in range(m)]
    elec = election(P2,borda_weights)
    w = np.argmax(elec.get_scores())
    elec.set_winner(w)
    axp = elec.find_smallest_AXp()
    return float(len(axp)/(n*m))

## Compute the margin of victory for the Borda score
def margin(P):
    P2 = P.votes.tolist()
    n, m = np.shape(P2)
    borda_weights = [m - i - 1 for i in range(m)]
    elec = election(P2, borda_weights)
    scores = elec.get_scores()
    m1 = -1
    m2 = -1
    for score in scores:
        if score>m1:
            m2=m1
            m1=score
        elif score>m2:
            m2=score
    return float(m1 - m2)

experiment = mapel.prepare_experiment(experiment_id=experiment_id,
                                      instance_type=instance_type,
                                      distance_id=distance_id,
                                      embedding_id=embedding_id)


# # EXPERIMENT INITIALIZATION
experiment.prepare_elections(printing=True)
print("Elections prepared")

# experiment.compute_distances(distance_id = distance_id)
# print("Distances computed")
# experiment.embed(algorithm='mds')
# print("Coordinates computed")
#
# experiment.compute_feature(feature_id="Agreement")
# print("Agreement computed")
#
# experiment.add_feature("SAXp_size", SAXp_size)
# experiment.compute_feature(feature_id='SAXp_size')
# print("SAXp_size computed")
## experiment.print_map_2d_colored_by_feature(feature_id='SAXp_size')
#
# experiment.add_feature("Margin", margin)
# experiment.compute_feature(feature_id='margin')
# print("Margin computed")

# # MAP ORIENTATION
# Will probably have to be manually tweak for different data
experiment = pp.upside_down_map(experiment)
experiment = pp.rotate_map(experiment, - math.pi *1.2, 'ID')

experiment.print_map(feature_id="SAXp_size",
                         # legend=True,
                         shading=True,
                         # title="SAXp_size",
                         # textual=['ID', 'UN', 'AN', 'ST'],
                         saveas="diversity_map_" + "SAXp_size")


# # TRIANGLE MAPS
pp.print_map_by_features(experiment, "Agreement", "SAXp_size", saveas='default')
pp.print_map_by_features(experiment, "Margin", "SAXp_size", saveas='default')


# # CORRELATIONS TO DISTANCES
pp.correlation_feature_feature(experiment, "SAXp_size", "Margin")
pp.correlation_feature_feature(experiment, "SAXp_size", "Agreement")
