# 'Abductive and Contrastive Explanations for Scoring Rules in Voting' from ECAI-24
This repository contains python implementation of algorithms as well as data used in the experimental part and some additionnal material associate to the paper 'Abductive and Contrastive Explanations for Scoring Rules in Voting' by Clément Contet, Umberto Grandi and Jérôme Mengin from ECAI-24.

## Appendix
`Appendix.pdf` is a two-page appendix giving more detailed proofs of some results presented in the paper.

The extended version of the paper (including this appendix) is available at https://arxiv.org/abs/2408.12927.

## Implementation
`scoring_rule_Xp.py` contains an implemetation of algorithms presented in the paper. The code was developped and tested under Python 3.11.

For the Minimal Hitting Set and SAT solvers we used solvers provided by the library `PySAT` more information on them can be find here https://pysathq.github.io/.

## Experiment
The experiment part uses tools developped for the paper 'Diversity, agreement, and polarization in elections' by Piotr Faliszewski, Andrzej Kaczmarczyk, Krzysztof Sornat, Stanisław Szufa, and Tomasz Wąs from IJCAI-23 available at https://github.com/Project-PRAGMA/diversity-agreement-polarization-IJCAI23.

The `experiments` directory already contains all the data that we used in our experiment. The data is there for convenience. The elections can be generated from scratch.

To run the experiment yourself, install the `dap` tool and simply add the directory `SAXp_size_4_12` and the script `SAXp_size_4_12.py`, uncomment parts of the script and run it.
