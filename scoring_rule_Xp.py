import numpy as np
from pysat.solvers import Solver
from pysat.examples.hitman import Hitman,Atom

### Model
# n : number of voters
# m : number of candidates indexed from 0 to m-1
# R : profile/rank matrix [ballot_0,ballot_1,...,ballot_{n-1}] where balllot_i is a list containing all candidates

### NW instance
# Object used to determin if a candidate is a NW under iterative modification of the partial rank matrix
class NW_instance:
    def __init__(self, R, v, w):
        # R: rank matrix
        self.R = R.astype(object)
        # n,m: #voters,#candidates
        (self.n,self.m) = np.shape(R)
        # v: scoring rule weights vector
        self.v = v
        # w: winning candidate
        self.w = w
        # Xp: current partial rank matrix representing an explanation
        self.Xp = None
        # partial_scores: scores associated to the last modified row of Xp
        self.partial_scores = np.full((self.n,self.m),None)
        # total scores: scores associated to the current Xp
        self.total_scores = np.full(self.m, None)
        # NW: boolean indicating if w is a NW
        self.NW = None
        # temp_XXX: temporary variables used to easily revert the action of freeing/fixing entry i0,j0
        self.temp_i0 = None
        self.temp_j0 = None
        self.temp_X_i0_j0 = None
        self.temp_partial_scores = None
        self.temp_total_scores = None
        self.temp_NW = None

    ## Determin if self.w is a necessary winner given S, the partial rank matrix used as a seed
    ## also set the scores attributes
    def is_NW(self,S):
        self.Xp = S.astype(object)
        for i in range(self.n):
            first_free = None
            last_free = None
            for j in range(self.m):
                c = self.Xp[i][j]
                if c != None:
                    self.partial_scores[i][c] = self.v[j]
                else:
                    if first_free == None:
                        first_free = j
                    if last_free == None or last_free < j:
                        last_free = j
            # if one element of self.partial_scores[i] is None, there is at least one free entry and first_free and last_free are set
            if self.partial_scores[i][self.w] == None:
                self.partial_scores[i][self.w] = self.v[last_free]
            for j in range(self.m):
                if self.partial_scores[i][j] == None:
                    self.partial_scores[i][j] = self.v[first_free]
        self.total_scores = np.sum(self.partial_scores, axis=0)
        self.NW = self.total_scores[self.w] >= np.max(self.total_scores)
        return self.NW

    ## Fix the entry i0,j0 in Xp, the partial rank matrix
    def fix_entry(self,i0,j0):
        self.Xp[i0][j0] = self.R[i0][j0]
        self.temp_X_i0_j0 = None
        self.temp_partial_scores = np.full(self.m,None)
        self.temp_i0 = i0
        self.temp_j0 = j0
        first_free = None
        last_free = None
        for j in range(self.m):
            c = self.Xp[i0][j]
            if c != None:
                self.temp_partial_scores[c] = self.v[j]
            else:
                if first_free == None:
                    first_free = j
                if last_free == None or last_free < j:
                    last_free = j
        # if one element of self.partial_scores[i] is None, there is at least one free entry and first_free and last_free are set
        if self.temp_partial_scores[self.w] == None:
            self.temp_partial_scores[self.w] = self.v[last_free]
        for j in range(self.m):
            if self.temp_partial_scores[j] == None:
                self.temp_partial_scores[j] = self.v[first_free]
        self.temp_total_scores = self.total_scores + self.temp_partial_scores - self.partial_scores[i0]
        self.temp_NW = self.temp_total_scores[self.w] >= np.max(self.temp_total_scores)
        return self.temp_NW

    ## Free the entry i0,j0 in Xp, the partial rank matrix
    def free_entry(self,i0,j0):
        self.Xp[i0][j0] = None
        self.temp_X_i0_j0 = self.R[i0][j0]
        self.temp_partial_scores = np.full(self.m,None)
        self.temp_i0 = i0
        self.temp_j0 = j0
        first_free = None
        last_free = None
        for j in range(self.m):
            c = self.Xp[i0][j]
            if c != None:
                self.temp_partial_scores[c] = self.v[j]
            else:
                if first_free == None:
                    first_free = j
                if last_free == None or last_free < j:
                    last_free = j
        # if one element of self.partial_scores[i] is None, there is at least one free entry and first_free and last_free are set
        if self.temp_partial_scores[self.w] == None:
            self.temp_partial_scores[self.w] = self.v[last_free]
        for j in range(self.m):
            if self.temp_partial_scores[j] == None:
                self.temp_partial_scores[j] = self.v[first_free]
        self.temp_total_scores = self.total_scores + self.temp_partial_scores - self.partial_scores[i0]
        self.temp_NW = self.temp_total_scores[self.w] >= np.max(self.temp_total_scores)
        return self.temp_NW

    ## Push the ongoing modification of Xp (free/fix)
    def push(self):
        self.total_scores = self.temp_total_scores
        self.partial_scores[self.temp_i0] = self.temp_partial_scores
        self.NW = self.temp_NW
        self.temp_i0 = None
        self.temp_j0 = None
        self.temp_X_i0_j0 = None
        self.temp_partial_scores = None
        self.temp_total_scores = None
        self.temp_NW = None

    ## Revert the ongoing modification of Xp (free/fix)
    def revert(self):
        self.Xp[self.temp_i0][self.temp_j0] = self.temp_X_i0_j0
        self.temp_i0 = None
        self.temp_j0 = None
        self.temp_X_i0_j0 = None
        self.temp_partial_scores = None
        self.temp_total_scores = None
        self.temp_NW = None


### election
# Object used to represent an election (Profile + weights vector of the scoring rule)
class election:
    def __init__(self,R,v):
        self.w = None
        self.R = R.astype(object)
        self.v = v
        (self.n,self.m) = np.shape(R)
        self.final_scores = self.get_scores()

    ## Compute score of a profile with a given scoring rule
    def get_scores(self):
        scores = np.zeros(self.m, dtype=int)
        for ballot in self.R:
            for j in range(self.m):
                scores[ballot[j]] += self.v[j]
        return scores

    ## Set the winner which wil be used for computing the explanation
    def set_winner(self,w):
        if self.final_scores[w] < max(self.get_scores()):
            print("The given candidate is not a winner in the election")
        else:
            self.w = w

    ## Compute the complementary of a rank matrix S
    def get_complementary(self,S):
        return np.where(S != None, None, self.R)

    ## Set the environment to run find_CXp_aux
    def find_CXp(self,S):
        instance = NW_instance(self.R, self.v, self.w)
        if instance.is_NW(S):
            print("Error: w in NW(S)")
        else:
            return self.find_CXp_aux(instance)

    ## Computes a CXp [Algorithm 1]
    def find_CXp_aux(self,instance):
        set = np.argwhere(instance.Xp == None)
        np.random.shuffle(set) # any ordering of the entries is valid, here we use a random one
        for [i,j] in set:
            instance.fix_entry(i,j)
            if instance.temp_NW:
                instance.revert()
            else:
                instance.push()
        return self.get_complementary(instance.Xp)

    ## Set the environment to run find_AXp_aux
    def find_AXp(self,S):
        instance = NW_instance(self.R, self.v, self.w)
        if not instance.is_NW(S):
            print("Error: w not in NW(S)")
        else:
            return self.find_AXp_aux(instance)

    ## Computes a AXp [Algorithm 2]
    def find_AXp_aux(self,instance):
        set = np.argwhere(instance.Xp != None)
        np.random.shuffle(set) # any ordering of the entries is valid, here we use a random one
        for [i, j] in set:
            if None not in instance.Xp[i]:
                instance.free_entry(i, j)
                instance.push()
                self.ensure_Irr(instance,i,j) # elements in set which could be removed from the for loop are not, it does not break things but adds useless iterations
            else:
                instance.free_entry(i,j)
                if not instance.temp_NW:
                    instance.revert()
                else:
                    instance.push()
        return instance.Xp

    ## Ensure that the AXp is irredundant [Algorithm 3]
    def ensure_Irr(self,instance,i,j0):
        set2 = np.argwhere(instance.Xp[i] != None)
        np.random.shuffle(set2)
        for j in set2:
            instance.free_entry(i,j)
            if not instance.temp_NW:
                instance.revert()
            else:
                instance.push()
                return
        instance.fix_entry(i,j0)
        instance.push()

    ## Enumerate all possible AXp and CXp using a SAT solver [Algorithm 4]
    def enum_xp(self):
        list_axp = []
        list_cxp = []
        s = Solver()
        # a_{i,j} is encoded as l_{m*i+j+1} and l_{m*i+j+1} = True means that a_{i,j} is known
        # first we remove the impossible points where every position but one is fixed in a ballot
        for i in range(self.n):
            for j in range(self.m):
                s.add_clause([-(self.m * i + k + 1) if k != j else (self.m * i + k + 1) for k in range(self.m)])
        while s.solve():
            instance = NW_instance(self.R, self.v, self.w)
            if instance.is_NW(self.sol_to_seed(s.get_model())):
                axp = self.find_AXp_aux(instance)
                list_axp.append(axp)
                clause = list(self.axp_to_clause(axp))
                s.add_clause(clause)
            else:
                cxp = self.find_CXp_aux(instance)
                list_cxp.append(cxp)
                clause = list(self.cxp_to_clause(cxp))
                s.add_clause(clause)
        s.delete()
        return list_axp, list_cxp

    ## Convert a solution returned by the SAT solver to a seed
    def sol_to_seed(self,sol):
        mask = np.reshape(np.array(sol),(self.n,self.m))
        return np.where(mask<0,None,self.R)

    ## Convert an AXp to a clause accepted by the SAT solver
    def axp_to_clause(self,axp):
        return [int(-(self.m * k[0] + k[1] + 1)) for k in np.argwhere(axp != None)]

    ## Convert a CXp to a clause accepted by the SAT solver
    def cxp_to_clause(self,cxp):
        return [int(self.m * k[0] + k[1] + 1) for k in np.argwhere(cxp != None)]

    ## Compute a smallest AXp by computing minimal hitting sets
    def find_smallest_AXp(self):
        # a_{i,j} is encoded as l_{m*i+j+1} and l_{m*i+j+1} = True means that a_{i,j} is known
        # first we remove the impossible points where every position but one is fixed in a ballot
        constraints = [[Atom(self.m*i+k+1,False) if k != j else Atom(self.m*i+k+1,True) for k in range(self.m)] for i in range(self.n) for j in range(self.m)]
        hitman = Hitman(subject_to=constraints,htype='sorted')
        while True:
            instance = NW_instance(self.R, self.v, self.w)
            S = self.sol_to_seed(hitman.get())
            if instance.is_NW(S):
                hitman.delete()
                return S
            else:
                cxp = self.find_CXp_aux(instance)
                clause = list(self.cxp_to_clause(cxp))
                hitman.hit(clause)
